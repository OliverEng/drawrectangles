﻿namespace DrawRectangles;

internal class Program
{
    private static void Main(string[] args)
    {
        var getInput = true;
        var columns = 0;
        var rows = 0;

        // Try to get input, if input is false, keep trying...
        while (getInput)
        {
            Console.WriteLine($"This program will draw rectangles within rectangles. \n");
            Console.WriteLine("Enter height of rectangle:");
            var inputHeight = Console.ReadLine();
            Console.WriteLine("Enter width of rectangle:");
            var inputWidth = Console.ReadLine();
            try
            {
                rows = int.Parse(inputHeight!);
                columns = int.Parse(inputWidth!);
                getInput = false;
            }
            catch (Exception)
            {
                Console.WriteLine("Invalid Input! Must be a whole number.");
            }
        }

        // Generate the rectangle array
        var rectangle = GenerateRectangle(rows, columns);

        // Display the rectangle array to the console
        DrawRectangle(rows, columns, rectangle);
    }

    // Generate and populate the rectangle within rectangles array...
    private static char[,] GenerateRectangle(int rows, int columns)
    {
        // Create empty 2D array for the rectangle
        var rectangle = new char[rows, columns];

        Console.WriteLine($"Creating rectangles within rectangles. Outer rectangle dimensions: {rows}x{columns}\n");

        // Generate the top and bottom half of the rectangle
        for (var y = 0; y < rows / 2; y += 2)
        {
            rectangle = GenerateRectTopBottom(rectangle, y);
        }

        // Generate the left and right half of the rectangle
        for (var x = 0; x < columns / 2; x += 2)
        {
            rectangle = GenerateRectLeftRight(rectangle, x);
        }

        return rectangle;
    }

    // Generate the top and the bottom of the rectangle in 2D array with offset...
    public static char[,] GenerateRectTopBottom(char[,] rectangle, int offset)
    {
        var cols = rectangle.GetLength(1)-1;
        var rows = rectangle.GetLength(0)-1;

        // Go through columns top to bottom with offsets and insert char in rows
        for (var x = offset; x <= cols - offset; x++)
        {
            // Make sure there are no single '*'
            if (offset != cols - offset)
            {
                rectangle[offset, x] = '*';
            }
        }

        // Go through columns bottom to top with offsets and insert char in rows
        for (var x = offset; x <= cols - offset; x++)
        {
            // Make sure there are no single '*'
            if (offset != cols - offset)
            {
                rectangle[rows - offset, x] = '*';
            }
        }

        return rectangle;
    }

    // Generate the left and right of the rectangle in 2D array with offset...
    public static char[,] GenerateRectLeftRight(char[,] rectangle, int offset)
    {
        var cols = rectangle.GetLength(1)-1;
        var rows = rectangle.GetLength(0)-1;

        // Go through rows left to right with offsets and insert char in column
        for (var y = offset; y <= rows - offset; y++)
        {
            // Make sure there are no single '*'
            if (offset != rows - offset)
            {
                rectangle[y, offset] = '*';
            }
        }

        // Go through rows right to left with offsets and insert char in column
        for (var y = offset; y <= rows - offset; y++)
        {
            // Make sure there are no single '*'
            if (offset != rows - offset)
            {
                rectangle[y, cols - offset] = '*';
            }
        }

        return rectangle;
    }

    private static void DrawRectangle(int rows, int columns, char[,] rectangle)
    {
        for (var x = 0; x < rows; x++)
        {
            for (var y = 0; y < columns; y++)
            {
                Console.Write(rectangle[x, y] == 0 ? $"  " : $" {rectangle[x, y]}");
            }

            Console.Write($"\n");
        }
    }
}